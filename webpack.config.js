module.exports = {
    mode: "development",
    entry: "./client/src/start.js",
    devServer: {
        host: '0.0.0.0',
        port: 8090,
        publicPath: '/',
        historyApiFallback: true
    },
    output: {
        path: __dirname + "/public/build",
        filename: 'bundle.js'
    },
    node: {
        fs: "empty"
    },
    devtool: 'sourc-emap',
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loaders: ['babel-loader']
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "less-loader"
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    }
};