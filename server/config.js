const config = {
    app: {
        port: process.env.APP_PORT
    },
    mongo_db: {
        host: process.env.MONGO_HOST,
        port: process.env.MONGO_PORT,
        name: process.env.MONGO_DB_NAME
    },
};

module.exports = config;