import mongoose from "mongoose";

import '../models/article';
import '../models/paragraph';
import '../models/suggestion';
import {
    STATUS_PENDING,
    STATUS_APPROVED,
} from '../models/suggestion'

import config from '../config';
const { mongo_db: { host, port, name } } = config;
const connectionString = `mongodb://${host}:${port}/${name}`;

const Article = mongoose.model('article');
const Paragraph = mongoose.model('paragraph');
const Suggestion = mongoose.model('suggestion');

const PAGINATION_PER_PAGE = 10;

export function connect() {
    mongoose.connect(connectionString, { useNewUrlParser: true }, function(err) {
        if (err) throw err;
    });
}

export function getArticleLists(params, callback) {
    let suggestionStatus = params.showApproved === 'true' ? STATUS_APPROVED : STATUS_PENDING;
    let suggestionExpr;

    suggestionExpr = {status: {$eq: suggestionStatus}};
    let perPage = PAGINATION_PER_PAGE;
    let page = params.page || 1;

    return Article
        .find()
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .populate(
            {
                path: 'paragraphs',
                populate: {
                    path: 'suggestions',
                    match: suggestionExpr
            }
        }).exec(function (err, articles) {
            Article.count().exec(function(err, count) {
                if (err) callback({});
                let data = {
                    data: articles,
                    current: page,
                    pages: Math.ceil(count / perPage),
                    total: count
                };
                callback(data);
            })
        });
}

export function createArticle(url) {
    const article = new Article({
        article_url: url,
    });

    return article.save();
}

export function save(document) {
    return document.save();
}

export function bulkCreateParagraph(paragraphs, articleId) {
    let paragraphObjects = [];
    paragraphs.forEach(paragraph => {
        const paragraphDocument = new Paragraph({
            article_id: articleId,
            text: paragraph,
        });
        paragraphObjects.push(paragraphDocument)
    });

    return Paragraph.create(paragraphObjects);
}

export function createSuggestion(data, callback) {
    const suggestion = new Suggestion(data);
    suggestion.save().then(suggestion => {
        Paragraph.findById(data.paragraph_id).then(paragraph => {
            paragraph.suggestions.push(suggestion);
            paragraph.save().then(paragraph => {
                Article.findById(paragraph.article_id).then(article => {
                    callback(article)
                });
            }).catch(error => {
                callback({}, error)
            });
        });
    });
}

export function updateSuggestion(data, callback) {
    Suggestion.findOneAndUpdate({ _id: data._id }, {
        $set: data
    }).then(suggestion => {
        callback(suggestion)
    }).catch(error => {
        callback({}, error)
    });
}

export function updateParagraph(data, callback) {
    Paragraph.findOneAndUpdate({ _id: data._id }, {
        $set: data
    }).then(paragraph => {
        Article.findById(paragraph.article_id).then(article => {
            callback(article)
        }).catch(error => {
            callback({}, error)
        });
    });
}