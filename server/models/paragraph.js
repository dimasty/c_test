import mongoose from "mongoose";

const Schema = mongoose.Schema;

const ParagraphSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, auto: true},
    article_id: {type: Schema.Types.ObjectId, ref: 'article'},
    text: String,
    improved_text: String,
    suggestions: [{
        type: Schema.Types.ObjectId, ref: 'suggestion'
    }]
});

const Paragraph = mongoose.model('paragraph', ParagraphSchema);