import mongoose from "mongoose";

const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, auto: true},
    article_url: String,
    paragraphs: [{
        type: Schema.Types.ObjectId, ref: 'paragraph'
    }]
});

const Article = mongoose.model('article', ArticleSchema);