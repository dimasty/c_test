import mongoose from "mongoose";

const Schema = mongoose.Schema;

export const STATUS_PENDING = 0;
export const STATUS_APPROVED = 1;
export const STATUS_DECLINED = 2;


const SuggestionSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, auto: true},
    paragraph_id: {type: Schema.Types.ObjectId, ref: 'paragraph'},
    text: String,
    status: {type: Schema.Types.Number, default: STATUS_PENDING},
    date: { type: Date, default: Date.now },
});

const Suggestion = mongoose.model('suggestion', SuggestionSchema);