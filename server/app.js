import express from "express";
import jsdom from "jsdom";
import request from "request";
import cors from "cors";
import bodyParser from "body-parser";
import * as mongoDb from "./utils/mongo-util";
import config from './config';
const { app: { port } } = config;

const { JSDOM } = jsdom;

const HTTP_BAD_REQUEST = 400;
const HTTP_SERVER_ERROR = 500;

mongoDb.connect();
const app = express();

app.use(bodyParser.json());
app.use(cors({ origin: '*' }));

app.get('/parse', (req, res) => {
    let result = {
        data: []
    };
    const url = req.query.url;
    if (!url) {
        res.status(HTTP_BAD_REQUEST);
        result.error = 'Parameter url must be provided';
        res.send(result);
    }
    request({ uri:url }, function (error, response, body) {
        let paragraphs = [];
        if (error) {
            res.status(HTTP_BAD_REQUEST);
            result.error = `Error when loading page data from: ${url}`;
            res.send(result);
            return
        }
        let article = mongoDb.createArticle(url);
        article.then(article => {
            if (article._id === undefined) {
                res.status(HTTP_BAD_REQUEST);
                result.error = `Something wrong`;
                res.send(result);
                return;
            }
            const dom = new JSDOM(body);
            dom.window.document.body.querySelectorAll('p').forEach(function(p) {
                paragraphs.push(p.textContent);
            });
            mongoDb.bulkCreateParagraph(paragraphs, article._id).then(newParagraphs => {
                newParagraphs.map(paragraph => {
                   article.paragraphs.push(paragraph);
                });
                mongoDb.save(article).then(article => {
                    result.data = [article];
                    res.send(result);
                })
            });
        });

    });
});

app.get('/results', (req, res) => {
    let result = {
        data: []
    };
   mongoDb.getArticleLists(req.query, function (data, err) {
       if (err) {
           result.error = err;
           res.status = HTTP_BAD_REQUEST;
       }
       res.send(data);
   });
});

app.post('/suggestion', (req, res) => {
    const result = {data: []};
    let data = req.body.data;
    if (!data) {
        result.error = `Wrong data provided`;
        res.status(HTTP_BAD_REQUEST);
        res.send(result);
    }
    mongoDb.createSuggestion(data, function (relatedArticle, error) {
        if (error) {
            result.error = error;
            res.status(HTTP_SERVER_ERROR);
            res.send(result);
        }
        result.data = relatedArticle;
        res.send(result);
    });
});

app.put('/suggestion', (req, res) => {
    const result = {data: []};
    let data = req.body.data;
    if (!data) {
        result.error = `Wrong data provided`;
        res.status(HTTP_BAD_REQUEST);
        res.send(result);
    }
    mongoDb.updateSuggestion(data, function (suggestion, error) {
        if (error) {
            result.error = error;
            res.status(HTTP_SERVER_ERROR);
            res.send(result);
        }
        result.data = suggestion;
        res.send(result);
    });
});

app.put('/paragraph', (req, res) => {
    const result = {data: []};
    let data = req.body.data;

    if (!data) {
        result.error = `Wrong data provided`;
        res.status(HTTP_BAD_REQUEST);
        res.send(result);
    }
    mongoDb.updateParagraph(data, function (relatedArticle, error) {
        if (error) {
            result.error = error;
            res.status(HTTP_SERVER_ERROR);
            res.send(result);
        }
        result.data = relatedArticle;
        res.send(result);
    });
});

const server = app.listen(port, () => {
   console.log(`Server is up and running on port ${port}`);
});
