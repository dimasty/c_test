#Ciklum test SPA

### How to start
* [Install docker](https://docs.docker.com/engine/installation/)

* Run docker-compose:

```
$ docker-compose up -d
```

* Backend API server will be available at [localhost:8084]()
* Client part available at [localhost:8090]()

* Get article paragraphs page:

```
localhost:8090/paragraphs?url=https://www.dagbladet.no/mat/for-en-kul-iskrem-men-du-bor-kanskje-ikke-spise-den-pa-forste-date/70102917

```
Each API call will save new article info. Click on Article paragraphs to see list of parsed paragraphs

* Get article results page:

```
localhost:8090/results?showApproved=[true/false]

```
If showApproved will set in true state you will see only approved suggestion in suggestion section
