import React, {PureComponent} from "react"
import { addAdminSuggestion } from "../actions/articleActions";
import {connect} from "react-redux";

class AddSuggestion extends PureComponent{

    constructor(props) {
        super(props);
        this.state = {
            suggestionText: '',
            paragraph: this.props.paragraph,
            error: null,
            loading: false,
            currentParagraphId: null,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.response.suggestion.loading !== this.state.loading) {
            this.setState({
                loading: nextProps.response.suggestion.loading
            });
        }
    };

    handleChange = (event) => {
        this.setState({
            suggestionText: event.target.value,
            error: null
        });
    };

    handleSendBtnClick = (paragraphId) => {
        this.setState({
            currentParagraphId: paragraphId
        });
        if (this.state.suggestionText === "") {
            this.setState({
                error: "Suggestion can not be empty"
            });
            return;
        }
        this.props.onAprroveSuggestion(this.state.paragraph._id);
        this.props.dispatch(addAdminSuggestion(this.state.paragraph._id, this.state.suggestionText))
    };

    render() {
        const error = this.state.error && <span className="suggestion-error" >{this.state.error}</span>;
        return (
            <section className="add-suggestion-section">
                <div className="card-body text-monospace">
                    {error}
                    <input type="text" className="form-control" onChange={this.handleChange} value={this.state.suggestionText} placeholder="Set custom improve" required/>
                    <button onClick={this.handleSendBtnClick.bind(this, this.state.paragraph._id)} className="btn btn-link">Send</button>
                </div>
            </section>
        )
    }
}

export default connect(
    state => ({
        response: state,
    }),
)(AddSuggestion);
