import React, {PureComponent} from 'react'
import Paragraph from '../paragraph'
import AdminParagraph from '../admin-paragraph'

export default class ParagraphList extends PureComponent {

    state = {
        paragraphs: this.props.paragraphs
    };

    render() {
        const paragraphElements = this.state.paragraphs.map(paragraph =>
            <div key={paragraph._id} className="">
                <li className="paragraph-list__li">
                    {this.getParagraphComponentByPath(paragraph)}
                </li>
            </div>
        );
        return (
            <section className="paragraph-section">
                <div className="container">
                    <ul className="">{paragraphElements}</ul>
                </div>
            </section>
        )
    }

    getParagraphComponentByPath = (paragraph) => {
        const pathName = window.location.pathname;
        const isAdminPage = pathName.match(/results/g);
        if (isAdminPage) {
           return <AdminParagraph paragraph = {paragraph} />
        } else {
            return  <Paragraph paragraph={paragraph} />
        }
    }
}