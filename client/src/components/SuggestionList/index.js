import React, {PureComponent} from 'react'
import Suggestion from '../suggestion'

export default class SuggestionList extends PureComponent {
    render() {
        const {suggestions, onDeclineClick, onAcceptClick} = this.props;
        const suggestionElements = suggestions.map(suggestion =>
          <div key={suggestion._id} className="suggestion-list__ul">
                <li className="suggestion-list__li">
                    <Suggestion
                        suggestion = {suggestion}
                        onDeclineButtonClick = {onDeclineClick.bind(this, suggestion._id)}
                        onAcceptButtonClick = {onAcceptClick.bind(this, suggestion._id)}
                    />
                </li>
            </div>
        );
        return (
            <ul className="card-body">{suggestionElements}</ul>
        )
    }
}