import React, {PureComponent} from "react"
import { addSuggestion } from "../actions/articleActions";
import {connect} from "react-redux";

class AddSuggestion extends PureComponent{

    constructor(props) {
        super(props);
        this.state = {
            suggestionText: '',
            paragraph: this.props.paragraph,
            error: null,
            loading: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.relatedArticle.suggestion.loading !== this.state.loading) {
            this.setState({
                loading: nextProps.relatedArticle.suggestion.loading,
                suggestionText: ''
            });
        }
    }

    handleChange = (event) => {
        this.setState({
            suggestionText: event.target.value,
            error: null
        });
    };

    handleSendBtnClick = (paragraphId) => {
        if (this.state.suggestionText === "") {
            this.setState({
                error: "Suggestion can not be empty"
            });
            return;
        }
        this.props.dispatch(addSuggestion(paragraphId, this.state.suggestionText))
    };

    render() {
        const error = this.state.error && <span className="suggestion-error">{this.state.error}</span>;

        return (
            <section className="add-suggestion-section">
                <div style={{width: '100%'}}>
                    <div className="card-body text-monospace">
                        {error}
                        <input type="text" className="form-control" onChange={this.handleChange} value={this.state.suggestionText} placeholder="Add your suggestion" required/>
                        <button onClick={this.handleSendBtnClick.bind(this, this.state.paragraph._id)} className="btn btn-link">Send</button>
                    </div>
                </div>
            </section>

        )
    }
}

export default connect(
    state => ({
        relatedArticle: state,
    }),
)(AddSuggestion);
