import React, {PureComponent} from "react"
import {connect} from "react-redux";
import AddSuggestion from "./add-suggestion";

class Paragraph extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            paragraph: this.props.paragraph,
            paragraphText: this.props.paragraph.text,
        };
    }

    render() {
        const paragraph = this.state.paragraph;
        return (
            <section className="paragraph-section">
                <div className="border-bottom">
                    <section style={{ cursor: "pointer"}} className="card-body">
                        <div className="input-group">
                            <div>
                                <span className="font-weight-bold">Text:</span>
                                <p>{this.state.paragraphText}</p>
                            </div>
                            <AddSuggestion paragraph={paragraph} />
                        </div>
                    </section>
                </div>
            </section>
        )
    }
}

export default connect(
    state => ({
        results: state.results
    }),
)(Paragraph);