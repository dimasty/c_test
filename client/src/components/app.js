import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import ArticleApp from './article-app';
import ParagraphApp from "./paragraph-app";
import "bootstrap/dist/css/bootstrap.css"
import "../css/index.less"

class App extends Component {
    render() {
        return (
            <div>
                <Route path="/results" component={ArticleApp}/>
                <Route path="/paragraphs" component={ParagraphApp}/>
            </div>
        );
    }
}

export default App