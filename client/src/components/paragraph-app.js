import React, {PureComponent} from "react"
import {parseArticle} from "../actions/articleActions";
import {connect} from "react-redux";
import ArticleList from "./ArticleList";

class ParagraphApp extends PureComponent {
    componentDidMount() {
        const url = this.props.location.search;
        this.props.dispatch(parseArticle(url));
    }

    render() {
        console.log(this.props);
        if (this.props.article.error) {
            return <div className="container"> {this.props.article.error} </div>
        }
        return (
            <div className="container">
                <ArticleList articles = {this.props.article.items}/>
            </div>
        )
    }
}

export default connect(
    state => ({
        article: state.parse
    }),
)(ParagraphApp);