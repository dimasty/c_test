import React, {PureComponent} from 'react'
import Article from '../article'


class ArticleList extends PureComponent {

    render() {
        const articles = this.props.articles;
        const articleElements = articles.map(article =>
            <li key={article._id} className="article-list__li border-bottom">
                <Article
                    article = {article}
                />
            </li>
        );
        return (
            <ul className="list-group">{articleElements}</ul>
        )
    }
}

export default ArticleList
