import React, {PureComponent} from "react"
import ArticleList from "./ArticleList";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import { fetchArticles } from "../actions/articleActions";

const ITEMS_COUNT_PER_PAGE = 10;

class ArticleApp extends PureComponent {

    constructor(props) {
        super(props);
        const urlParams = new URLSearchParams(window.location.search);
        const showApproved = urlParams.get('showApproved') || false;
        this.state = {
            activePage: 1,
            showApproved: showApproved
        };
    }

    handlePageChange(pageNumber) {
        this.setState({activePage: pageNumber});
        let params = [];
        params["showApproved"] = this.state.showApproved;
        params["page"] = pageNumber;
        this.props.dispatch(fetchArticles(params));
    }

    componentDidMount() {
        let params = [];
        params['showApproved'] = this.state.showApproved;
        this.props.dispatch(fetchArticles(params));
    }

    render() {
        const response = this.props.results.items;
        const articles = response.data || [];
        const total = response.total || 0;
        if (total === 0) {
            return (
                <div>No data</div>
            )
        }
        return (
            <div className="container border">
                <ArticleList articles = {articles}/>
                <div className="pagination-container">
                    <div className="pagination">
                        <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={ITEMS_COUNT_PER_PAGE}
                            totalItemsCount={total}
                            pageRangeDisplayed={5}
                            onChange={::this.handlePageChange}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        results: state.results
    }),
)(ArticleApp);