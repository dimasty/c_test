import React, {PureComponent} from "react"
import {Link} from "react-router-dom";
import ParagraphList from "./ParagraphList"

class Article extends PureComponent {

    state = {
        isParagraphsOpen: false,
    };

    render() {
        const {article} = this.props;
        const paragraphList = this.state.isParagraphsOpen && <ParagraphList paragraphs={article.paragraphs}/>
        return (
            <div>
                <div className="card-body text-monospace">
                    <p>Article url: <a href={article.article_url} target="_blank">{article.article_url}</a></p>
                    <button onClick={this.handleParagraphBtnClick} className="btn btn-outline-info">
                        Article paragraphs:
                    </button>
                    {paragraphList}
                </div>
            </div>
        )
    }

    handleParagraphBtnClick = () => {
        this.setState({
            isParagraphsOpen: !this.state.isParagraphsOpen
        })
    };
}

export default Article