import React, {PureComponent} from "react"
import {connect} from "react-redux";
import ParagraphList from "./ParagraphList";

class ParseApp extends PureComponent{
    render() {
        return (
            <div className="container">
                <ParagraphList  paragraphs = {this.props.results}/>
            </div>
        )
    }
}

export default connect(
    state => ({
        results: state.results
    }),
)(ParseApp);