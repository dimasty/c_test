import React, {PureComponent} from "react"

class Suggestion extends PureComponent{

    render() {
        const {suggestion, onDeclineButtonClick, onAcceptButtonClick} = this.props;
        const body = <section className="card-text">{suggestion.text}</section>;
        return (
            <div className="mx-auto">
                {body}
                <button onClick={onAcceptButtonClick} className="btn btn-link">Accept</button>
                <button onClick={onDeclineButtonClick} className="btn btn-link decline-button">Decline</button>
            </div>
        )
    }
}

export default Suggestion