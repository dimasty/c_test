import React, {PureComponent} from "react"
import SuggestionList from "./SuggestionList";
import {connect} from "react-redux";
import AddAdminSuggestion from "./add-admin-suggestion";
import {updateSuggestion, updateParagraph} from "../actions/articleActions";

const STATUS_APPROVED = 1;
const STATUS_DECLINED = 2;

class AdminParagraph extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            isSuggestionsOpen: false,
            paragraph: this.props.paragraph,
            suggestions: this.props.paragraph.suggestions,
            paragraphText: this.props.paragraph.text,
            improvedText: this.props.paragraph.improved_text,
            isVisible: true
        };
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange = (event) => {
        this.setState({
            improvedText: event.target.value
        });
    };

    handleSuggestionBtnClick = () => {
        this.setState({
            isSuggestionsOpen: !this.state.isSuggestionsOpen
        })
    };

    declineClickHandler = () => function (paragraphId, suggestionId) {
        if (paragraphId === this.state.paragraph._id) {
            let that = this;
            const newSuggestions = [];
            this.state.suggestions.forEach(function (suggestion) {
                if (suggestion._id !== suggestionId) {
                    newSuggestions.push(suggestion)
                } else {
                    suggestion.status = STATUS_DECLINED;
                    that.props.dispatch(updateSuggestion(suggestion));
                }
            });
            this.setState({
                suggestions: newSuggestions
            })
        }
    };

    acceptClickHandler = () => function (paragraphId, suggestionId) {
        if (paragraphId === this.state.paragraph._id) {
            let that = this;
            const newSuggestions = [];
            let newText = '';
            this.state.suggestions.forEach(function (suggestion) {
                if (suggestion._id === suggestionId) {
                    newText = suggestion.text;
                    suggestion.status = STATUS_APPROVED;
                    that.props.dispatch(updateSuggestion(suggestion));
                }
            });
            that.setState({
                suggestions: newSuggestions,
                improvedText: newText,
                isVisible: false
            });
            const dataToUpdate = {
                _id: paragraphId,
                improved_text: newText
            };
            this.props.dispatch(updateParagraph(dataToUpdate));
        }
    };

    handleApprovedSuggestionParagraph = () => function (paragraphId) {
        if (paragraphId === this.state.paragraph._id) {
            this.setState({
                isVisible: false
            })
        }
    };

    render() {
        if (!this.state.isVisible) {
            return ('');
        }
        const paragraph = this.state.paragraph;
        const suggestionsCount = this.state.suggestions.length;
        const suggestionList = this.state.isSuggestionsOpen && suggestionsCount > 0 ?
            <SuggestionList
                suggestions={this.state.suggestions}
                onDeclineClick={this.declineClickHandler().bind(this, paragraph._id)}
                onAcceptClick={this.acceptClickHandler().bind(this, paragraph._id)}
            /> : null;

        const suggestionButton = suggestionsCount > 0 &&
            <button onClick={this.handleSuggestionBtnClick} className="btn btn-link">
                Suggestions ({suggestionsCount})
            </button>;
        return (
            <div className="border-bottom">
                <section className="card-body">
                    <div className="input-group">
                        <div>
                            <span className="font-weight-bold">Original text:</span>
                            <p>{this.state.paragraphText}</p>
                            <span className="font-weight-bold">Improved text:</span>
                            <p>{this.state.improvedText}</p>
                        </div>
                        <AddAdminSuggestion
                            paragraph={paragraph}
                            onAprroveSuggestion={this.handleApprovedSuggestionParagraph().bind(this, this.state.paragraph._id)}
                        />
                    </div>
                </section>
                {suggestionButton}
                {suggestionList}
            </div>
        )
    }
}

export default connect(
    state => ({
        results: state.results
    }),
)(AdminParagraph);