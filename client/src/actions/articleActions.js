import axios from 'axios';
import config from '../../config';
const { api_url } = config;

const axiosInstance = axios.create({
    baseURL: api_url,
});

export const FETCH_ARTICLES_BEGIN   = 'FETCH_ARTICLES_BEGIN';
export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_FAILURE = 'FETCH_ARTICLES_FAILURE';
export const PARSE_ARTICLE_BEGIN    = 'PARSE_ARTICLE_BEGIN';
export const PARSE_ARTICLE_SUCCESS  = 'PARSE_ARTICLE_SUCCESS';
export const PARSE_ARTICLE_FAILURE  = 'PARSE_ARTICLE_FAILURE';
export const ADD_SUGGESTION_BEGIN   = 'ADD_SUGGESTION_BEGIN';
export const ADD_SUGGESTION_SUCCESS   = 'ADD_SUGGESTION_SUCCESS';
export const ADD_SUGGESTION_FAILURE   = 'ADD_SUGGESTION_FAILURE';
export const UPDATE_SUGGESTION_BEGIN   = 'UPDATE_SUGGESTION_BEGIN';
export const UPDATE_SUGGESTION_SUCCESS   = 'UPDATE_SUGGESTION_SUCCESS';
export const UPDATE_SUGGESTION_FAILURE   = 'UPDATE_SUGGESTION_FAILURE';
export const UPDATE_PARAGRAPH_BEGIN   = 'UPDATE_PARAGRAPH_BEGIN';
export const UPDATE_PARAGRAPH_SUCCESS   = 'UPDATE_PARAGRAPH_SUCCESS';
export const UPDATE_PARAGRAPH_FAILURE   = 'UPDATE_PARAGRAPH_FAILURE';


export const fetchArticlesBegin = () => ({
    type: FETCH_ARTICLES_BEGIN
});

export const fetchArticlesSuccess = articles => ({
    type: FETCH_ARTICLES_SUCCESS,
    payload: { articles }
});

export const fetchArticlesFailure = error => ({
    type: FETCH_ARTICLES_FAILURE,
    payload: { error }
});

export const parseArticleBegin = () => ({
    type: PARSE_ARTICLE_BEGIN
});

export const parseArticleSuccess = parsed => ({
    type: PARSE_ARTICLE_SUCCESS,
    payload: { parsed }
});

export const parseArticleFailure = error => ({
    type: PARSE_ARTICLE_FAILURE,
    payload: { error }
});

export const addSuggestionBegin = () => ({
    type: ADD_SUGGESTION_BEGIN
});

export const addSuggestionSuccess = response => ({
    type: ADD_SUGGESTION_SUCCESS,
    payload: { response }
});

export const addSuggestionFailure = error => ({
    type: ADD_SUGGESTION_FAILURE,
    payload: { error }
});

export const updateSuggestionBegin = () => ({
    type: UPDATE_SUGGESTION_BEGIN
});

export const updateSuggestionSuccess = response => ({
    type: UPDATE_SUGGESTION_SUCCESS,
    payload: { response }
});

export const updateSuggestionFailure = error => ({
    type: UPDATE_SUGGESTION_FAILURE,
    payload: { error }
});

export const updateParagraphBegin = () => ({
    type: UPDATE_PARAGRAPH_BEGIN
});

export const updateParagraphSuccess = response => ({
    type: UPDATE_PARAGRAPH_SUCCESS,
    payload: { response }
});

export const updateParagraphFailure = error => ({
    type: UPDATE_PARAGRAPH_FAILURE,
    payload: { error }
});


export function fetchArticles(params) {
    let queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    return dispatch => {
        dispatch(fetchArticlesBegin());
        return axiosInstance.get(`/results?${queryString}`)
            .then(handleErrors)
            .then(response => {
                let articles = response.data;
                dispatch(fetchArticlesSuccess(articles));
                return articles;
            })
            .catch(error => dispatch(fetchArticlesFailure(error)));
    };
}

export function parseArticle(params) {
    return dispatch => {
        dispatch(parseArticleBegin());
        return axiosInstance.get(`/parse${params}`)
            .then(handleErrors)
            .then(response => {
                response = response.data;
                let articles = response.data;
                dispatch(parseArticleSuccess(articles));
                return articles;
            })
            .catch(error => dispatch(parseArticleFailure(error)));
    };
}

export function addSuggestion(paragraphId, suggestionText) {
    let data = {paragraph_id: paragraphId, text: suggestionText};
    return dispatch => {
        dispatch(addSuggestionBegin());
        return axiosInstance.post(`/suggestion`, {data: data})
            .then(handleErrors)
            .then(response => {
                dispatch(addSuggestionSuccess(response));
                return response
            })
            .catch(error => dispatch(addSuggestionFailure(error)));
    };
}

export function updateSuggestion(suggestion) {
    console.log(suggestion);
    return dispatch => {
        dispatch(updateSuggestionBegin());
        return axiosInstance.put(`/suggestion`, {data: suggestion})
            .then(handleErrors)
            .then(response => {
                dispatch(updateSuggestionSuccess(response));
                return response
            })
            .catch(error => dispatch(updateSuggestionFailure(error)));
    };
}

export function updateParagraph(paragraph) {
    return dispatch => {
        dispatch(updateParagraphBegin());
        return axiosInstance.put(`/paragraph`, {data: paragraph})
            .then(handleErrors)
            .then(response => {
                dispatch(updateParagraphSuccess(response));
                return response
            })
            .catch(error => dispatch(updateParagraphFailure(error)));
    };
}

export function addAdminSuggestion(paragraphId, suggestionText) {
    let data = {
        _id: paragraphId,
        improved_text: suggestionText
    };
    return dispatch => {
        dispatch(addSuggestionBegin());
        return axiosInstance.put(`/paragraph`, {data: data})
            .then(handleErrors)
            .then(response => {
                dispatch(addSuggestionSuccess(response));
                return response
            })
            .catch(error => dispatch(addSuggestionFailure(error)));
    };
}

function handleErrors(response) {
    if (response.status !== 200) {
        throw Error(response.statusText);
    }
    return response;
}