import { combineReducers } from "redux";
import results from "./results";
import parse from "./parse";
import suggestion from "./suggestion";
import paragraph from "./paragraph";
import {routerReducer} from "react-router-redux";

export default combineReducers({
    routing: routerReducer,
    results,
    parse,
    suggestion,
    paragraph
})