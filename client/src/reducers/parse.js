import {
    PARSE_ARTICLE_BEGIN,
    PARSE_ARTICLE_SUCCESS,
    PARSE_ARTICLE_FAILURE
} from '../actions/articleActions';

const initialState = {
    items: [],
    loading: false,
    error: null
};

export default function parseUrlReducer(state = initialState, action) {
    switch(action.type) {
        case PARSE_ARTICLE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };
        case PARSE_ARTICLE_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.parsed
            };
        case PARSE_ARTICLE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error.response.data.error,
                items: action.payload.error.response.data.data
            };
        default:
            return state;
    }
}