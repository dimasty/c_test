import {
    UPDATE_PARAGRAPH_BEGIN,
    UPDATE_PARAGRAPH_SUCCESS,
    UPDATE_PARAGRAPH_FAILURE
} from '../actions/articleActions';

const initialState = {
    response: {},
    loading: false,
    error: null
};

export default function paragraphReducer(state = initialState, action) {
    switch(action.type) {
        case UPDATE_PARAGRAPH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };
        case UPDATE_PARAGRAPH_SUCCESS:
            return {
                ...state,
                loading: false,
                response: action.payload.response
            };
        case UPDATE_PARAGRAPH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                response: {}
            };
        default:
            return state;
    }
}