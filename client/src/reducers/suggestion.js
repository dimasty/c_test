import {
    ADD_SUGGESTION_BEGIN,
    ADD_SUGGESTION_SUCCESS,
    ADD_SUGGESTION_FAILURE,
    UPDATE_SUGGESTION_BEGIN,
    UPDATE_SUGGESTION_SUCCESS,
    UPDATE_SUGGESTION_FAILURE
} from '../actions/articleActions';

const initialState = {
    response: {},
    loading: false,
    error: null
};

export default function suggestionReducer(state = initialState, action) {
    switch(action.type) {
        case ADD_SUGGESTION_BEGIN:

        case UPDATE_SUGGESTION_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };
        case ADD_SUGGESTION_SUCCESS:
        case UPDATE_SUGGESTION_SUCCESS:
            return {
                ...state,
                loading: false,
                response: action.payload.response
            };
        case ADD_SUGGESTION_FAILURE:
        case UPDATE_SUGGESTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                response: {}
            };
        default:
            return state;
    }
}